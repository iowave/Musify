# Musify

![Musify](image/featureGraphic.png)

![Musify](image/musifyPlay.png)


Musify is an Android application designed to control smart lights over wifi.
Download Now on [Goolge Play](https://play.google.com/store/apps/details?id=com.iowave.musify)

Features.
The Musify app is packed full of features like

- Lights Sync on Musical beats  
- Playing Local music on the phone 
- The Basic turn on/off, brightness control and auto animate on/off
- 16 million combination color picker 
- Choose from 17 Effects and 15 Color palette
- Multiple Device control on just one tap
- Device Status Online/Offline
- Device Add/Edit/Delete 
- Hardware support Esp8266 and Raspberry pi

### Setup


![Setup wifi](image/connectMusify.png)

There are two ways to configure smart lights on network 

1. Use Smart config( Only Esp8266)
2. Configure Esp8266 or Rpi on the wifi 


![Setting ](image/control.png)

## Color Pick
 

    {
       "color_pick": "126,255,241"  //R,G,B
       }

  
  ## Power
  
  - On  = true
  - Off = false
   

>     {
>         "power": false
>         }

  
 ## Effect 

 - 0=Music
 - 1=SolidColor
 - 2=Meteor Shower
 - 3=Blue Fire
 - 4=Color Chase
 - 5=Sparking
 - 6=Juggle
 - 7=Rainbow Wave
 - 8=Sparkling Rainbow
 - 9=Confetti
 - 10=Sinelon
 - 11=Beat SinWave
 - 12=Fire
 - 13=Water
 - 14=Party
 - 15=Sunrise
 - 16=Rainbow


>     {
>        "effect": 5
>         }

## Color Palette

 - 0=Rainbow Color
 - 1=Rainbow Stripe
 - 2=Cloud
 - 3=Lava
 - 4=Ocean
 - 5=Forest
 - 6=Party
 - 7=Heat
 - 8=OceanBreeze
 - 9=Landscape
 - 10=Departure
 - 11=Autumn
 - 12=EmeraldDragon
 - 13=IndianFlag
 - 14=American

>     {
>       "color_palette": 3
>     }


## Brightness	

- 0   = Min
- 255 = Max



>     {
>       "brightness": 87
>     }

## Speed

- 0   = Min
- 255 = Max

>     {
>       "speed": 87
>     }
		
## Cooling	

- 0   = Min
- 255 = Max	

>     {
>       "cooling": 87
>     }

## Sparking
- 0   = Min
- 255 = Max


>     {
>       "sparking": 87
>     }

## Auto Animate

- On  = true
- Off = false
	
       {
           "auto_animate": true
        }

## Auto Animate Speed

- 10  s = Min
- 300 s = Max
	
       {
           "auto_animate_speed": 10
        }


