#include <FastLED.h>
FASTLED_USING_NAMESPACE
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include <ArduinoJson.h>

extern "C" {
#include "user_interface.h"
}

#include "GradientPallets.h"

#define  Box_PIN  6
#define NUM_LEDS 16
#define FRAMES_PER_SECOND  120


uint8_t animateSpeed = 55;
uint8_t COOLING = 55;
uint8_t SPARKING = 120;
uint8_t frame = 0;
uint8_t  red, green, blue, va;
uint8_t hue = 32;
uint8_t currentEffectIndex = 1;

uint8_t currentPaletteIndex = 0;
uint8_t currentPatternIndex = 0;
uint8_t gHue = 0; // rotating "base color" used by many of the patterns
uint8_t brightness = 255;

byte idex = 255;
byte meteorLength = 50;

bool gReverseDirection;
bool animteState;
bool powerBox;

char data[100];

uint8_t autoAnimateSpeed = 10;// in sec

uint8_t currentAnimate = 2;

CRGB box[NUM_LEDS];
#define COLOR_ORDER GRB
#define LED_TYPE WS2812B
CRGB solidColor = CRGB::Blue;

const char *ssid = "Enter SSID NAME"; // The name of the Wi-Fi network that will be created
const char *pass = "Enter  Wifi Password";   // The password required to connect to it, leave blank for an open network

WebSocketsServer webSocket(80);    // create a websocket server on port 81


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("resetting");
  startWiFi();                 // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
  startWebSocket();            // Start a WebSocket server

  LEDS.addLeds<LED_TYPE, Box_PIN, COLOR_ORDER>(box, NUM_LEDS);
  fill_solid(box, NUM_LEDS, CRGB::Black);
  LEDS.setBrightness(brightness);
  FastLED.show();

}

void startWiFi() { // Start a Wi-Fi access point, and try to connect to some given access points. Then wait for either an AP or STA connection
  Serial.print("Access Point \"");
  Serial.print(ssid);
  Serial.println("\" started\r\n");

  WiFi.begin(ssid, pass);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {  // Wait for the Wi-Fi to connect
    delay(250);
    Serial.print('.');
  }
  Serial.println("\r\n");
}



void startWebSocket() { // Start a WebSocket server
  webSocket.begin();                          // start the websocket server
  webSocket.onEvent(webSocketEvent);          // if there's an incomming websocket message, go to function 'webSocketEvent'
  Serial.println("WebSocket server started.");
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) { // When a WebSocket message is received

  Serial.printf("WStype [%d]\r\n", type);

  switch (type) {
    case WStype_DISCONNECTED:             // if the websocket is disconnected
      Serial.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {              // if a new websocket connection is established
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        // webSocket.sendTXT(num, "Status", "OK");
      }
      break;
    case WStype_TEXT:                     // if new text data is received
      //Serial.println(payload);
      Serial.printf("[%u] get Text: %s\n", num, payload);
      String _payload = String((char *) &payload[0]);
      readJsonData(_payload, lenght);
      break;
  }
}


void readJsonData(String jsonMusify, size_t jsonMusifySize) {
  DynamicJsonDocument doc(100);

  Serial.println(jsonMusify);
  // Parse JSON object
  DeserializationError error = deserializeJson(doc, jsonMusify);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  JsonObject obj = doc.as<JsonObject>();

  // Extract values
  Serial.println(F("Response:"));

  if (obj.containsKey("power")) {
    powerBox = doc["power"];
    setPower(powerBox);

  } else if (obj.containsKey("color_pick")) {

    String b_color = doc["color_pick"];
    red = getValue (b_color, 0).toInt();
    green = getValue (b_color, 1).toInt();
    blue = getValue (b_color, 2).toInt();
    solidColor = CRGB(red, green, blue);
    Serial.printf("color red %d  green %d blue %d \n", red, green, blue );

  } else if (obj.containsKey("effect")) {

    int b_effect = doc["effect"];
    currentEffectIndex = b_effect;

    if (b_effect == 0 || b_effect == 1) {
      solidColor = CRGB(red, green, blue);
    }
    SetBrightness(brightness);

  } else if (obj.containsKey("color_palette")) {

    currentPaletteIndex = doc["color_palette"];

  } else if (obj.containsKey("brightness")) {

    brightness = doc["brightness"];
    SetBrightness(brightness);

  } else if (obj.containsKey("speed")) {

    animateSpeed = doc["speed"];

  } else if (obj.containsKey("cooling")) {

    COOLING = doc["cooling"];

  } else if (obj.containsKey("sparking")) {
    SPARKING = doc["sparking"];

  } else if (obj.containsKey("auto_animate")) {
    animteState = doc["auto_animate"];

  } else if (obj.containsKey("auto_animate_speed")) {

    autoAnimateSpeed = doc["auto_animate_speed"];
  } else {
    // do nothing
  }
  /////////power ////////////
  if (powerBox) {
    if (currentEffectIndex == 1) {
      SolidColorBox(true);
      // if music is not selected
      for (int i = 0; i < brightness ; i++) {
        FastLED.setBrightness(i);
        FastLED.show();
        delay(30);
      }
    } else if (currentEffectIndex == 0 ) {
      SolidColorBox(true);
    }
  } else {
    for (int i = brightness; i > 0 ; i--) {
      FastLED.setBrightness(i);
      FastLED.show();
      delay(30);
    }
    SolidColorBox(0);
    powerBox = 0 ;
    return;
  }
}

void loop() {
  webSocket.loop();                           // constantly check for websocket events
  // insert a delay to keep the framerate modest

  if (powerBox) {
    if (animteState) {
      AnimationCases(currentAnimate);
      EVERY_N_SECONDS(autoAnimateSpeed) {
        nextPattern();  // change patterns periodically
      }
    }  else {
      if (currentEffectIndex > 1) {
        AnimationCases(currentEffectIndex);
        Serial.printf("currentEffectIndex %d \n" , currentEffectIndex );
      }
    }
  }

  // send the 'leds' array out to the actual LED strip
  FastLED.show();

  //FastLED.delay(1000 / FRAMES_PER_SECOND);

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) {
    gHue++;
  } // slowly cycle the "base color" through the rainbow

}

void nextPattern() {
  currentAnimate++;
  if (currentAnimate > 17) {
    currentAnimate = 2;
  }
  Serial.printf("[%u] nextPattern!\n", currentAnimate);
}
void AnimationCases(byte cas) {
  Serial.println(cas);
  switch (cas) {
    case 0:
      SolidColorBox(1);
      break;
    case 2:
      meteorShower();
      break;
    case 3:
      FireBlu();
      break;
    case 4:
      ColorChase();
      break;
    case 5:
      Spark();
      break;
    case 6:
      juggle();
      break;
    case 7:
      rainbow();
      break;
    case 8:
      rainbowWithGlitter();
      break;
    case 9:
      confetti();
      break;
    case 10:
      sinelon();
      break;
    case 11:
      bpm();
      break;
    case 12:
      Fire();
      break;
    case 13:
      Water();
      break;
    case 14:
      pride();
      break;
    case 15:
      sunrise();
      break;
    case 16:
      rainbowSolid();
      break;
  }
}

void SetBrightness(byte a) {
  if (a) {
    FastLED.setBrightness(brightness);
  }
}
void SolidColorBox(boolean st) {
  if (st) {
    fill_solid(box, NUM_LEDS, solidColor);
  } else {
    fill_solid(box, NUM_LEDS, CRGB::Black);
  }
  FastLED.show();
}

/////////////////////////// Set Power ///////////////////////////
void setPower(bool value) {
  powerBox = value;
}

/////////////////////////////////////////////////////////////Solid Color /////////////////////////////////////////////////////
void SetSolidColor(byte sel , CRGB color)
{

}

/////////////////////////////////////////////////////////////////////// meteorShower ////////////////////////////////////

void meteorShower() {
  // slide all the pixels down one in the array
  memmove8( &box[1], &box[0], (NUM_LEDS - 1) * 3 );

  // increment the meteor display frame
  idex++;
  // make sure we don't drift into space
  if ( idex > meteorLength ) {
    idex = 0;
    // cycle through hues in each successive meteor tail
    hue += 32;
  }

  // this switch controls the actual meteor animation, i.e., what gets placed in the
  // first position and then subsequently gets moved down the strip by the memmove above
  switch ( idex ) {
    case 0:
      box[0] = CRGB(200, 200, 200);
      break;
    case 1:
      box[0] = CHSV((hue - 20), 255, 210);
      break;
    case 2:
      box[0] = CHSV((hue - 22), 255, 180);
      break;
    case 3:
      box[0] = CHSV((hue - 23), 255, 150);
      break;
    case 4:
      box[0] = CHSV((hue - 24), 255, 110);
      break;
    case 5:
      box[0] = CHSV((hue - 25), 255, 90);
      break;
    case 6:
      box[0] = CHSV((hue - 26), 160, 60);
      break;
    case 7:
      box[0] = CHSV((hue - 27), 140, 40);
      break;
    case 8:
      box[0] = CHSV((hue - 28), 120, 20);
      break;
    case 9:
      box[0] = CHSV((hue - 29), 100, 20);
      break;
    default:
      box[0] = CRGB::Black;
  }
  // show the blinky
  FastLED.show();
  // control the animation speed/frame rate
  delay(animateSpeed);
}

///////////////////////////////////////////////////////////////////////////BLUE FIRE //////////////////////////////////////////////////////

void FireBlu()
{
  // Add entropy to random number generator; we use a lot of it.
  random16_add_entropy( rand());

  Fire2012(); // run simulation frame

  FastLED.show(); // display this frame
  FastLED.delay(1000 / FRAMES_PER_SECOND);
}

void Fire2012() {
  // Array of temperature readings at each simulation cell
  static byte heat[NUM_LEDS];

  // Step 1.  Cool down every cell a little
  for ( int i = 0; i < NUM_LEDS; i++) {
    heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
  }

  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for ( int k = NUM_LEDS - 1; k >= 2; k--) {
    heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
  }

  // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
  if ( random8() < SPARKING ) {
    int y = random8(7);
    heat[y] = qadd8( heat[y], random8(100, 255) );
  }

  // Step 4.  Map from heat cells to LED colors
  for ( int j = 0; j < NUM_LEDS; j++) {
    // Scale the heat value from 0-255 down to 0-240
    // for best results with color palettes.
    byte colorindex = scale8( heat[j], 240);
    CRGB color = ColorFromPalette(IceColors_p, colorindex);
    int pixelnumber;
    if ( gReverseDirection ) {
      pixelnumber = (NUM_LEDS - 1) - j;
    } else {
      pixelnumber = j;
    }
    box[pixelnumber] = color;
  }
}

/////////////////////////////////////////////Color Chase ////////////////////////////////////

void ColorChase() {
  static uint8_t i;
  if (i <= 255) {
    for (uint16_t dot = 0; dot < NUM_LEDS; dot++) {
      box[dot] = ColorFromPalette( palettes[currentPaletteIndex], i ); // normal palette access
      FastLED.show();
      delay(animateSpeed);
    }
    i = i + 10;
  }
}


//////////////////////////////Sparking///////////////////////////

void Spark() {
  uint8_t stripLength = sizeof(box) / sizeof(CRGB);
  uint16_t rand = random16();
  for (int i = 0; i < stripLength; i++)
  {
    box[i].nscale8(245); //fade amount can go from 0 to 255
  }
  if (rand < (65536 / (256 - (constrain(animateSpeed, 1, 255)))))
  {
    box[rand % stripLength].setHSV(0, 0, 255);
  }
  FastLED.show();         //All animations are applied!..send the results to the strip(s)
  frame += animateSpeed;


}

////////////////////// juggle ////////////////////////////////////////////////////////////

void juggle()
{
  static uint8_t    numdots =   4; // Number of dots in use.
  static uint8_t   faderate =   2; // How long should the trails be. Very low value = longer trails.
  static uint8_t     hueinc =  255 / numdots - 1; // Incremental change in hue between each dot.
  static uint8_t    thishue =   0; // Starting hue.
  static uint8_t     curhue =   0; // The current hue
  static uint8_t    thissat = 255; // Saturation of the colour.
  static uint8_t thisbright = 255; // How bright should the LED/display be.
  static uint8_t   basebeat =   5; // Higher = faster movement.

  static uint8_t lastSecond =  99;  // Static variable, means it's only defined once. This is our 'debounce' variable.
  uint8_t secondHand = (millis() / 1000) % 30; // IMPORTANT!!! Change '30' to a different value to change duration of the loop.

  if (lastSecond != secondHand) { // Debounce to make sure we're not repeating an assignment.
    lastSecond = secondHand;
    switch (secondHand) {
      case  0: numdots = 1; basebeat = 20; hueinc = 16; faderate = 2; thishue = 0; break; // You can change values here, one at a time , or altogether.
      case 10: numdots = 4; basebeat = 10; hueinc = 16; faderate = 8; thishue = 128; break;
      case 20: numdots = 8; basebeat =  3; hueinc =  0; faderate = 8; thishue = random8(); break; // Only gets called once, and not continuously for the next several seconds. Therefore, no rainbows.
      case 30: break;
    }
  }

  // Several colored dots, weaving in and out of sync with each other
  curhue = thishue; // Reset the hue values.
  fadeToBlackBy(box, NUM_LEDS, faderate);
  for ( int i = 0; i < numdots; i++) {
    //beat16 is a FastLED 3.1 function
    box[beatsin16(basebeat + i + numdots, 0, NUM_LEDS - 1)] += CHSV(gHue + curhue, thissat, thisbright);
    curhue += hueinc;
  }
}

////////////////////////RainBow/////////////////////////////

void rainbow() {
  // FastLED's built-in rainbow generator
  fill_rainbow( box, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter() {
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void rainbowSolid() {
  fill_solid(box, NUM_LEDS, CHSV(gHue, 255, 255));
}


void addGlitter( fract8 chanceOfGlitter) {
  if ( random8() < chanceOfGlitter) {
    box[ random16(NUM_LEDS) ] += CRGB::White;
  }
}


void confetti() {
  // random colored speckles that blink in and fade smoothlyw
  fadeToBlackBy( box, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  // box[pos] += CHSV( gHue + random8(64), 200, 255);
  box[pos] += ColorFromPalette(palettes[currentPaletteIndex], gHue + random8(64));

}

void sinelon() {
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( box, NUM_LEDS, 20);
  int pos = beatsin16(animateSpeed, 0, NUM_LEDS - 1);
  // box[pos] += ColorFromPalette(palettes[currentPaletteIndex], gHue, 255);
  static int prevpos = 0;
  CRGB color = ColorFromPalette(palettes[currentPaletteIndex], gHue, 255);
  if ( pos < prevpos ) {
    fill_solid( box + pos, (prevpos - pos) + 1, color);
  } else {
    fill_solid( box + prevpos, (pos - prevpos) + 1, color);
  }
  prevpos = pos;
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  //  uint8_t BeatsPerMinute = 62;
  //  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  //  for ( int i = 0; i < NUM_LEDS_B; i++) { //9948
  //    box[i] = ColorFromPalette(palettes[currentPaletteIndex], gHue + (i * 2), beat - gHue + (i * 10));
  //  }
  uint8_t beat = beatsin8( animateSpeed, 64, 255);
  CRGBPalette16 palette = palettes[currentPaletteIndex];
  for ( int i = 0; i < NUM_LEDS; i++) {
    box[i] = ColorFromPalette(palette, gHue + (i * 2), beat - gHue + (i * 10));
  }
}

void Fire()
{
  heatMap(HeatColors_p, true);
}

void Water()
{
  heatMap(IceColors_p, true);
}

// Pride2015 by Mark Kriegsman: https://gist.github.com/kriegsman/964de772d64c502760e5
// This function draws rainbows with an ever-changing,
// widely-varying set of parameters.
void pride()
{
  static uint16_t sPseudotime = 0;
  static uint16_t sLastMillis = 0;
  static uint16_t sHue16 = 0;

  uint8_t sat8 = beatsin88( 87, 220, 250);
  uint8_t brightdepth = beatsin88( 341, 96, 224);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 1, 3000);

  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5, 9);
  uint16_t brightnesstheta16 = sPseudotime;

  for ( uint16_t i = 0 ; i < NUM_LEDS; i++) {
    hue16 += hueinc16;
    uint8_t hue8 = hue16 / 256;

    brightnesstheta16  += brightnessthetainc16;
    uint16_t b16 = sin16( brightnesstheta16  ) + 32768;

    uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
    uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
    bri8 += (255 - brightdepth);

    CRGB newcolor = CHSV( hue8, sat8, bri8);

    uint16_t pixelnumber = i;
    pixelnumber = (NUM_LEDS - 1) - pixelnumber;

    nblend( box[pixelnumber], newcolor, 64);
  }
}


// based on FastLED example Fire2012WithPalette: https://github.com/FastLED/FastLED/blob/master/examples/Fire2012WithPalette/Fire2012WithPalette.ino
void heatMap(CRGBPalette16 palette, bool up)
{
  fill_solid(box, NUM_LEDS, CRGB::Black);

  // Add entropy to random number generator; we use a lot of it.
  random16_add_entropy(random(256));

  // Array of temperature readings at each simulation cell
  static byte heat[256];

  byte colorindex;

  // Step 1.  Cool down every cell a little
  for ( uint16_t i = 0; i < NUM_LEDS; i++) {
    heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
  }

  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for ( uint16_t k = NUM_LEDS - 1; k >= 2; k--) {
    heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
  }

  // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
  if ( random8() < SPARKING ) {
    int y = random8(7);
    heat[y] = qadd8( heat[y], random8(160, 255) );
  }

  // Step 4.  Map from heat cells to LED colors
  for ( uint16_t j = 0; j < NUM_LEDS; j++) {
    // Scale the heat value from 0-255 down to 0-240
    // for best results with color palettes.
    colorindex = scale8(heat[j], 190);

    CRGB color = ColorFromPalette(palette, colorindex);

    if (up) {
      box[j] = color;
    }
    else {
      box[(NUM_LEDS - 1) - j] = color;
    }
  }
}

void sunrise() {

  // total sunrise length, in minutes
  static const uint8_t sunriseLength = 30;

  // how often (in seconds) should the heat color increase?
  // for the default of 30 minutes, this should be about every 7 seconds
  // 7 seconds x 256 gradient steps = 1,792 seconds = ~30 minutes
  static const uint8_t interval = (sunriseLength * 60) / 256;

  // current gradient palette color index
  static uint8_t heatIndex = 0; // start out at 0

  // HeatColors_p is a gradient palette built in to FastLED
  // that fades from black to red, orange, yellow, white
  // feel free to use another palette or define your own custom one
  CRGB color = ColorFromPalette(HeatColors_p, heatIndex);

  // fill the entire strip with the current color
  fill_solid(box, NUM_LEDS, color);

  // slowly increase the heat
  EVERY_N_SECONDS(interval) {
    // stop incrementing at 255, we don't want to overflow back to 0
    if (heatIndex < 255) {
      heatIndex++;
    }
    if (heatIndex == 255) {
      heatIndex = 0;
    }
  }
}

String getValue(String data, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;
  char dataPart = ',';      //variable to hole the return text

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == dataPart || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  String val =  found > index ? data.substring(strIndex[0], strIndex[1]) : "";
  return val;
}
