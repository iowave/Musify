#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

CRGBPalette16 IceColors_p = CRGBPalette16(CRGB::Black, CRGB::Blue, CRGB::Aqua, CRGB::White);

DEFINE_GRADIENT_PALETTE( es_emerald_dragon_08_gp ) {
  0,  97, 255,  1,
  101,  47, 133,  1,
  178,  13, 43,  1,
  255,   2, 10,  1
};

DEFINE_GRADIENT_PALETTE( es_autumn_19_gp ) {
  0,  26,  1,  1,
  51,  67,  4,  1,
  84, 118, 14,  1,
  104, 137, 152, 52,
  112, 113, 65,  1,
  122, 133, 149, 59,
  124, 137, 152, 52,
  135, 113, 65,  1,
  142, 139, 154, 46,
  163, 113, 13,  1,
  204,  55,  3,  1,
  249,  17,  1,  1,
  255,  17,  1,  1
};

DEFINE_GRADIENT_PALETTE( departure_gp ) {
  0,   8,  3,  0,
  42,  23,  7,  0,
  63,  75, 38,  6,
  84, 169, 99, 38,
  106, 213, 169, 119,
  116, 255, 255, 255,
  138, 135, 255, 138,
  148,  22, 255, 24,
  170,   0, 255,  0,
  191,   0, 136,  0,
  212,   0, 55,  0,
  255,   0, 55,  0
};

DEFINE_GRADIENT_PALETTE( es_landscape_64_gp ) {
  0,   0,  0,  0,
  37,   2, 25,  1,
  76,  15, 115,  5,
  127,  79, 213,  1,
  128, 126, 211, 47,
  130, 188, 209, 247,
  153, 144, 182, 205,
  204,  59, 117, 250,
  255,   1, 37, 192
};

DEFINE_GRADIENT_PALETTE( es_ocean_breeze_036_gp ) {
  0,   1,  6,  7,
  89,   1, 99, 111,
  153, 144, 209, 255,
  255,   0, 73, 82
};

const TProgmemPalette16 IndianFlagPalette_p PROGMEM =
{
  CRGB::Orange,
  CRGB::Gray, // 'white' is too bright compared to red and blue
  CRGB::Green,
  CRGB::Black,

  CRGB::Orange,
  CRGB::Gray,
  CRGB::Green,
  CRGB::Black,

  CRGB::Orange,
  CRGB::Orange,
  CRGB::Gray,
  CRGB::Gray,
  CRGB::Green,
  CRGB::Green,
  CRGB::Black,
  CRGB::Black
};

const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
  CRGB::Red,
  CRGB::Gray, // 'white' is too bright compared to red and blue
  CRGB::Blue,
  CRGB::Black,

  CRGB::Red,
  CRGB::Gray,
  CRGB::Blue,
  CRGB::Black,

  CRGB::Red,
  CRGB::Red,
  CRGB::Gray,
  CRGB::Gray,
  CRGB::Blue,
  CRGB::Blue,
  CRGB::Black,
  CRGB::Black
};

const CRGBPalette16 palettes[] = {
  RainbowColors_p,
  RainbowStripeColors_p,
  CloudColors_p,
  LavaColors_p,
  OceanColors_p,
  ForestColors_p,
  PartyColors_p,
  HeatColors_p,
  es_ocean_breeze_036_gp,
  es_landscape_64_gp,
  departure_gp,
  es_autumn_19_gp,
  es_emerald_dragon_08_gp,
  IndianFlagPalette_p,
  myRedWhiteBluePalette_p
};

const uint8_t paletteCount = ARRAY_SIZE(palettes);
const uint8_t EffectPaletteCount = 16;
